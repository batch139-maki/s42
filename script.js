console.log("DOM MANIPULATION");

/*
	This is a WEB PAGE

	<html>
		<head>
			<title></title>
		</head>
		<body>
			<div>
				<h1></h1>
				<p></p>
			</div>
		</body>
	</html>
*/

/*DOM
	
	DOM = {
		document: {
			html: {
				head: {
					title: {
	
					},
					meta: {
	
					}
				},
				body: {
					div: {
						h1: "",
						p: ""
					}
				}
			}
		}
	}

*/

//Finding HTML Elements
	console.log(document.getElementById("demo"))
	console.log(document.getElementsByTagName("h1"))
	console.log(document.getElementsByClassName("title"))

	console.log(document.querySelector("#demo"))
	console.log(document.querySelector("h1"))
	console.log(document.querySelector(".title"))


let myH1 = document.querySelector(".title");
myH1.innerHTML = `Hello World`;

document.getElementById("demo").innerHTML = "Please enter your name";

document.querySelector(".title").style.color = "red"

//Event Listener
//element.addEventListener("event", cb())

let firstName = document.getElementById("txt-first-name");
let lastName = document.getElementById("txt-last-name");
let fullName = document.getElementById("span-full-name")

let updateName = () => {
    let firstTxt = firstName.value;
    let lastTxt = lastName.value;

    fullName.innerHTML = `${firstTxt} ${lastTxt}`
}

firstName.addEventListener("keyup", updateName);
lastName.addEventListener("keyup", updateName);
